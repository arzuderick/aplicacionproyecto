/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.cooperativa_abc;
import java.sql.*;
import java.util.Calendar;
import javax.swing.JOptionPane;
/**
 *
 * @author Derick Arzu
 */
public class UserLogin extends javax.swing.JFrame {
    static MySQL bdd = new MySQL();
    String current_user = "";
    /**
     * Creates new form UserLogin
     */
    public UserLogin() {
        initComponents();
    }
    
    public static void insertAdmin (Connection conn) {
        Calendar fechaActual = Calendar.getInstance();
        String currDate = Integer.toString(fechaActual.get(Calendar.YEAR)) + "-" + Integer.toString(fechaActual.get(Calendar.MONTH) + 1) + "-" + Integer.toString(fechaActual.get(Calendar.DATE));
        PreparedStatement stmt = null;
        try{
            stmt = conn.prepareStatement("INSERT INTO afiliados VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String admin = "admin";
                int codigo = 0;
                int tipo = 1;
                stmt.setInt(1,codigo);
                stmt.setString(6, currDate);
                stmt.setString(13, currDate);
                stmt.setInt(16,tipo);
                stmt.setString(17, currDate);
                stmt.setString(18, currDate);
                for(int i = 2; i <= 20; i++){
                    if(i == 6 || i ==13 || i == 17 || i == 18 || i ==16)
                        continue;
                    stmt.setString(i, admin);
                }
                stmt.executeUpdate();
        }
        catch (Exception exc) {exc.printStackTrace();}    
    }
    
    public static boolean iniciarSesion (String nombre_usuario, String clave, Connection conn){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select nombre_usuario, clave from afiliados where nombre_usuario = ?");
            myStmt.setString(1, nombre_usuario);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                if(myRs.getString("nombre_usuario").equals(nombre_usuario) && myRs.getString("clave").equals(clave))
                    return true;
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
        return false;
    }
    
    public static int getCodigoAfiliado (String nombre_usuario, Connection conn){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        int codigo = -1;
        try{
            myStmt = conn.prepareStatement("select codigo from afiliados where nombre_usuario = ?");
            myStmt.setString(1, nombre_usuario);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                codigo = myRs.getInt("codigo");
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
        return codigo;
    }
    
    public static String getInfoAfiliado (int codigo, Connection conn, int opcion){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        String info = "";
        try{
            myStmt = conn.prepareStatement("call sp_afiliados_read(?)");
            myStmt.setInt(1, codigo);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                switch(opcion){
                    case 1:
                        info = Integer.toString(myRs.getInt("codigo"));
                    break;
                    case 2: 
                        info = myRs.getString("primer_nombre") + " " + myRs.getString("segundo_nombre") + " " + myRs.getString("primer_apellido") + " " + myRs.getString("segundo_apellido") ; 
                    break;
                    case 3:
                        info = myRs.getString("fecha_nacimiento");
                    break;
                }
            }
        }
        catch (Exception exc) {exc.printStackTrace();}
        return info;
    }
    
    public static int getPermiso (String nombre_usuario, Connection conn){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select tipo from afiliados where nombre_usuario = ?");
            myStmt.setString(1, nombre_usuario);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                if(myRs.getInt("tipo") == 1)
                    return 1;
                else if (myRs.getInt("tipo") == 2)
                    return 2;
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
        return -1;
    }
    
    public static boolean existeAfiliado (String nombre_usuario, Connection conn){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select nombre_usuario from afiliados where nombre_usuario = ?");
            myStmt.setString(1, nombre_usuario);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                if (myRs.getString("nombre_usuario").equals(nombre_usuario))
                    return true;
            }
        }
        catch (Exception exc) {exc.printStackTrace();}
        return false;
    }
    
    public static int getNumeroCuenta (int codigo, Connection conn, String tipo){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        int numero_cuenta = -1;
        try{
            myStmt = conn.prepareStatement("select numero_cuenta from cuentas where codigo = ? and tipo = ?");
            myStmt.setInt(1, codigo);
            myStmt.setString(2, tipo);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                numero_cuenta = myRs.getInt("numero_cuenta");
            }
        }
        catch (Exception exc) {exc.printStackTrace();}
        return numero_cuenta;
    }
    
    public void setCurrentUser(String nombre_usuario){
        current_user = nombre_usuario;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainScreen_title = new javax.swing.JLabel();
        mainScreen_subtitle = new javax.swing.JLabel();
        mainScreen_fieldusuario = new javax.swing.JTextField();
        mainScreen_lblusuario = new javax.swing.JLabel();
        mainScreen_lblclave = new javax.swing.JLabel();
        mainScreen_fieldclave = new javax.swing.JPasswordField();
        mainScreen_btnlogin = new javax.swing.JButton();
        mainScreen_btnclose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainScreen_title.setFont(new java.awt.Font("Gill Sans MT", 1, 24)); // NOI18N
        mainScreen_title.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mainScreen_title.setText("Cooperativa ABC");

        mainScreen_subtitle.setFont(new java.awt.Font("Gill Sans MT", 2, 18)); // NOI18N
        mainScreen_subtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mainScreen_subtitle.setText("Iniciar Sesion");

        mainScreen_fieldusuario.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        mainScreen_fieldusuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mainScreen_fieldusuarioActionPerformed(evt);
            }
        });

        mainScreen_lblusuario.setFont(new java.awt.Font("Gill Sans MT", 0, 14)); // NOI18N
        mainScreen_lblusuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mainScreen_lblusuario.setText("Nombre de Usuario");

        mainScreen_lblclave.setFont(new java.awt.Font("Gill Sans MT", 0, 14)); // NOI18N
        mainScreen_lblclave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mainScreen_lblclave.setText("Clave");

        mainScreen_btnlogin.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        mainScreen_btnlogin.setText("Iniciar Sesion");
        mainScreen_btnlogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mainScreen_btnloginActionPerformed(evt);
            }
        });

        mainScreen_btnclose.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        mainScreen_btnclose.setText("Cerrar");
        mainScreen_btnclose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mainScreen_btncloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainScreen_title, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainScreen_subtitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainScreen_lblusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainScreen_lblclave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(209, 209, 209)
                .addComponent(mainScreen_fieldusuario, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(209, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mainScreen_btnlogin, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mainScreen_fieldclave, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(209, 209, 209))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(mainScreen_btnclose)
                        .addGap(249, 249, 249))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(mainScreen_title)
                .addGap(18, 18, 18)
                .addComponent(mainScreen_subtitle)
                .addGap(18, 18, 18)
                .addComponent(mainScreen_lblusuario)
                .addGap(18, 18, 18)
                .addComponent(mainScreen_fieldusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(mainScreen_lblclave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mainScreen_fieldclave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mainScreen_btnlogin)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mainScreen_btnclose)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mainScreen_fieldusuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mainScreen_fieldusuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mainScreen_fieldusuarioActionPerformed

    private void mainScreen_btncloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mainScreen_btncloseActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_mainScreen_btncloseActionPerformed

    private void mainScreen_btnloginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mainScreen_btnloginActionPerformed
        String nombre_usuario = mainScreen_fieldusuario.getText();
        String clave = String.valueOf(mainScreen_fieldclave.getPassword());
        Connection myConn = bdd.getMySQL();
        if(iniciarSesion(nombre_usuario,clave,myConn)){
            int permiso = getPermiso(nombre_usuario, myConn);
            setCurrentUser(nombre_usuario);
            System.out.println(current_user);
            if(permiso == 1){
                AdminInterface a_interface = new AdminInterface();
                a_interface.setVisible(true);
                a_interface.setLocationRelativeTo(null);
                this.dispose();
                a_interface.setCurrUser(current_user);
            }
            else if(permiso == 2){
                UserInterface u_interface = new UserInterface();
                u_interface.setVisible(true);
                u_interface.setLocationRelativeTo(null);
                this.dispose();
                int codigoAfiliado = getCodigoAfiliado(nombre_usuario, myConn);
                int numero_cuenta_ap = getNumeroCuenta(codigoAfiliado, myConn, "APORTACIONES");
                int numero_cuenta_ar = getNumeroCuenta(codigoAfiliado, myConn, "AHORROR");
                u_interface.setNumCuenta(numero_cuenta_ap, 1);
                u_interface.setNumCuenta(numero_cuenta_ar, 2);
                String nombre_afiliado = getInfoAfiliado(codigoAfiliado, myConn, 2);
                String codigo_afiliado = getInfoAfiliado(codigoAfiliado, myConn, 1);
                String fecha_nacimiento = getInfoAfiliado(codigoAfiliado, myConn, 3);
                u_interface.setNombreAfiliado(nombre_afiliado);
                u_interface.setCodigoAfiliado(codigo_afiliado);
                u_interface.setFechaNacimientoAfiliado(fecha_nacimiento);
            }
        }
        else{
            mainScreen_fieldusuario.setText("");
            mainScreen_fieldclave.setText("");
            JOptionPane.showMessageDialog(null, "Nombre de usuario o clave incorrecta");
        }
    }//GEN-LAST:event_mainScreen_btnloginActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        boolean existeAdmin = false;
        Connection myConn = bdd.getMySQL();
            existeAdmin = existeAfiliado ("admin", myConn);
            if(!existeAdmin)
                insertAdmin(myConn);

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UserLogin mainScreen = new UserLogin();
                mainScreen.setVisible(true);
                mainScreen.setLocationRelativeTo(null);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton mainScreen_btnclose;
    private javax.swing.JButton mainScreen_btnlogin;
    private javax.swing.JPasswordField mainScreen_fieldclave;
    public static javax.swing.JTextField mainScreen_fieldusuario;
    private javax.swing.JLabel mainScreen_lblclave;
    private javax.swing.JLabel mainScreen_lblusuario;
    private javax.swing.JLabel mainScreen_subtitle;
    private javax.swing.JLabel mainScreen_title;
    // End of variables declaration//GEN-END:variables
}
